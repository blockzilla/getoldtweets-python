#!/usr/bin/env python
import pika
import sys
import os
import getopt
import datetime
import codecs
import json
if sys.version_info[0] < 3:
    import got
else:
    import got3 as got


def main(argv):

    def queueTweet(t):
        channel.basic_publish(exchange='', routing_key=queue, body=t)
        print("writing tweet")

    host = os.environ.get('RABBITMQ_CONNECTION_STRING')
    queue = os.environ.get('RABBITMQ_TWEET_QUEUE')

    print("Connecting to host: " + host)

    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=host))
    channel = connection.channel()

    channel.queue_declare(queue=queue)

    if len(argv) == 0:
        print('You must pass some parameters. Use "-h" to help.')
        return

    if len(argv) == 1 and argv[0] == '-h':
        # f = open('exporter_help_text.txt', 'r')
        # print f.read()
        # f.close()

        return

    try:
        (opts, args) = getopt.getopt(argv, '', (
            'username=',
            'near=',
            'within=',
            'since=',
            'until=',
            'querysearch=',
            'toptweets',
            'maxtweets=',
            'daysback=',
            'output=',
        ))

        tweetCriteria = got.manager.TweetCriteria()
        outputFileName = 'output_got.csv'

        for (opt, arg) in opts:
            if opt == '--username':
                tweetCriteria.username = arg
            elif opt == '--since':

                tweetCriteria.since = arg
            elif opt == '--until':

                tweetCriteria.until = arg
            elif opt == '--querysearch':

                tweetCriteria.querySearch = arg
            elif opt == '--toptweets':

                tweetCriteria.topTweets = True
            elif opt == '--maxtweets':

                tweetCriteria.maxTweets = int(arg)
            elif opt == '--near':

                tweetCriteria.near = '"' + arg + '"'
            elif opt == '--within':

                tweetCriteria.within = '"' + arg + '"'
            elif opt == '--within':

                tweetCriteria.within = '"' + arg + '"'
            elif opt == '--output':

                outputFileName = arg
            elif opt == '--daysback':
                d = datetime.datetime.today() - datetime.timedelta(days=int(arg))
                tweetCriteria.since = d.strftime('%Y-%m-%d')
                tweetCriteria.until = datetime.datetime.today().strftime('%Y-%m-%d')

        print('Searching...\n')

        def receiveBuffer(tweets):
            for t in tweets:
                print(t.username)
                jsonTweet = {"username": t.username, "date": t.date.strftime(
                    '%Y-%m-%d %H:%M'), "retweets": t.retweets, "favorites": t.favorites, "text": t.text, "geo": t.geo, "mentions": t.mentions, "hashtags": t.hashtags, "id": t.id, "permalink": t.permalink}
                queueTweet(json.dumps(jsonTweet))

            print('More %d saved on file...\n' % len(tweets))
        got.manager.TweetManager.getTweets(tweetCriteria, receiveBuffer)

    except arg:
        print('Arguments parser error, try -h' + arg)
    finally:
        connection.close()


if __name__ == '__main__':
    main(sys.argv[1:])
